﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace English.Migrations
{
    public partial class initiail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VerbRows",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Translate = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VerbRows", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "IrrVerbs",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Form = table.Column<int>(nullable: false),
                    Word = table.Column<string>(nullable: true),
                    Wrong1 = table.Column<string>(nullable: true),
                    Wrong2 = table.Column<string>(nullable: true),
                    Wrong3 = table.Column<string>(nullable: true),
                    VerbRowId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IrrVerbs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IrrVerbs_VerbRows_VerbRowId",
                        column: x => x.VerbRowId,
                        principalTable: "VerbRows",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IrrVerbs_VerbRowId",
                table: "IrrVerbs",
                column: "VerbRowId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IrrVerbs");

            migrationBuilder.DropTable(
                name: "VerbRows");
        }
    }
}
