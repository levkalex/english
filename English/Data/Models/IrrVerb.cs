﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace English.Data.Models
{
    public class IrrVerb
    {
        public int Id { get; set; }
        public int Form { get; set; }
        public string Word { get; set; }

        public string Wrong1 { get; set; }
        public string Wrong2 { get; set; }
        public string Wrong3 { get; set; }

        public int? VerbRowId { get; set; }
        [JsonIgnore]
        public VerbRow VerbRow { get; set; }
    }
}
