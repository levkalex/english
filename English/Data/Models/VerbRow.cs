﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace English.Data.Models
{
    public class VerbRow
    {
        public int Id { get; set; }
        public string Translate { get; set; }

        public ICollection<IrrVerb> IrrVerbs { get; set; }
        public VerbRow()
        {
            IrrVerbs = new List<IrrVerb>();
        }
    }
}
