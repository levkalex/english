﻿using English.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace English.Data.Interfaces
{
    public interface IVerbRepository
    {
        VerbRow GetByWord(string infinitive);
        VerbRow GetRandomWord();
        List<VerbRow> GetRandomWords(int count);
        bool CheckAnswer(int vRowId, int form, string answer);
    }
}
