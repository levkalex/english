﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace English.Data.Identity
{
    public class User : IdentityUser<int>
    {
        public byte[] Avatar { get; set; }
    }
}