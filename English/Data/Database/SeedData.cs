﻿using English.Data.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace English.Data.Database
{
    static class SeedData
    {
        static ApplicationDbContext context;

        public static void EnsurePopulated(IApplicationBuilder applicationBuilder)
        {
            using (var serviceScope = applicationBuilder.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>();

                context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[VerbRows] ON");

                context.VerbRows.RemoveRange(context.VerbRows);

                context.VerbRows.Add(new VerbRow { Id = 1, Translate = "Идти" });
                context.VerbRows.Add(new VerbRow { Id = 2, Translate = "Ехать" });
                context.VerbRows.Add(new VerbRow { Id = 3, Translate = "Есть" });

                context.SaveChanges();
                context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[VerbRows] OFF");


                context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[IrrVerbs] ON");

                context.IrrVerbs.RemoveRange(context.IrrVerbs);

                context.IrrVerbs.Add(new IrrVerb { Id = 1, Form = 1, VerbRowId = 1, Word = "go", Wrong1 = "went", Wrong2 = "let", Wrong3 = "gone" });
                context.IrrVerbs.Add(new IrrVerb { Id = 2, Form = 2, VerbRowId = 1, Word = "went", Wrong1 = "go", Wrong2 = "run", Wrong3 = "gone" });
                context.IrrVerbs.Add(new IrrVerb { Id = 3, Form = 3, VerbRowId = 1, Word = "gone", Wrong1 = "went", Wrong2 = "go", Wrong3 = "gane" });

                context.IrrVerbs.Add(new IrrVerb { Id = 4, Form = 1, VerbRowId = 2, Word = "drive", Wrong1 = "drove", Wrong2 = "drivi", Wrong3 = "driven" });
                context.IrrVerbs.Add(new IrrVerb { Id = 5, Form = 2, VerbRowId = 2, Word = "drove", Wrong1 = "drov", Wrong2 = "drave", Wrong3 = "drive" });
                context.IrrVerbs.Add(new IrrVerb { Id = 6, Form = 3, VerbRowId = 2, Word = "driven", Wrong1 = "drive", Wrong2 = "drivenn", Wrong3 = "drove" });

                context.IrrVerbs.Add(new IrrVerb { Id = 7, Form = 1, VerbRowId = 3, Word = "eat", Wrong1 = "eats", Wrong2 = "iat", Wrong3 = "eaat" });
                context.IrrVerbs.Add(new IrrVerb { Id = 8, Form = 2, VerbRowId = 3, Word = "ate", Wrong1 = "eat", Wrong2 = "ete", Wrong3 = "atte" });
                context.IrrVerbs.Add(new IrrVerb { Id = 9, Form = 3, VerbRowId = 3, Word = "eaten", Wrong1 = "eatten", Wrong2 = "eat", Wrong3 = "eats" });

                context.SaveChanges();
                context.Database.ExecuteSqlQuery("SET IDENTITY_INSERT [dbo].[IrrVerbs] OFF");
            }

        }
    }

}
