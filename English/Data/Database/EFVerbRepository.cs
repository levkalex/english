﻿using English.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using English.Data.Interfaces;

namespace English.Data.Database
{
    public class EFVerbRepository : IVerbRepository
    {
        private ApplicationDbContext context;

        public EFVerbRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public VerbRow GetByWord(string infinitive)
        {
            return context.VerbRows.Include(x => x.IrrVerbs).Where(x => x.Id == x.IrrVerbs.FirstOrDefault(y => y.Word == infinitive).VerbRowId).FirstOrDefault();
        }

        public VerbRow GetRandomWord()
        {
            int countRow = context.VerbRows.Count();

            int rnd = new Random().Next(0, countRow);

            return context.VerbRows.Include(x => x.IrrVerbs).Skip(rnd).First();
        }

        public List<VerbRow> GetRandomWords(int count = 1)
        {
            int countRow = context.VerbRows.Count();
            List<VerbRow> verbRows = new List<VerbRow>();

            for (int i = 0; i < count; i++)
            {
                int rnd = new Random().Next(0, countRow);

                VerbRow verbRow = context.VerbRows.Include(x => x.IrrVerbs).Skip(rnd).First();
                if (!verbRows.Contains(verbRow))
                {
                    verbRows.Add(verbRow);
                }
                else
                    i--;
            };

            return verbRows;
        }

        public bool CheckAnswer(int vRowId, int form, string answer)
        {
           var verb = context.IrrVerbs.Where(x => x.VerbRowId == vRowId && x.Form == form).FirstOrDefault();
            if (verb.Word.ToUpper() == answer.ToUpper()) return true;
            else return false;
        }
    }
}
