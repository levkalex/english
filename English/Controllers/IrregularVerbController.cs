﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using English.Data.Interfaces;

namespace English.Controllers
{

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class IrregularVerbController : Controller
    {
        IVerbRepository verbRepository;
        public IrregularVerbController(IVerbRepository verbRepository)
        {
            this.verbRepository = verbRepository;
        }

        [HttpGet]
        public JsonResult get()
        {
            var result = verbRepository.GetRandomWords(3);

            return Json(result);
        }

        [HttpGet]
        public JsonResult check(int vRowId, int form, string answer)
        {
            var result = verbRepository.CheckAnswer(vRowId, form, answer);

            return Json(result);
        }
    }
}