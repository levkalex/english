﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using English.Data.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using English.Data.Interfaces;
using English.Data.Identity;
using Microsoft.AspNetCore.Identity;

namespace English
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(
                Configuration["Data:English:ConnectionString"]));

            services.AddDbContext<IdentityDbContext>(options => options.UseSqlServer(
              Configuration["Data:EnglishIdentity:ConnectionString"]));

            services.AddIdentity<User, Role>()
              .AddRoleManager<RoleManager<Role>>()
              .AddUserManager<WUserManager>()
              .AddEntityFrameworkStores<IdentityDbContext>()
              .AddDefaultTokenProviders();

            services.AddTransient<IVerbRepository, EFVerbRepository>();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseAuthentication();
            app.UseDeveloperExceptionPage();
            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();

            SeedData.EnsurePopulated(app);
        }
    }
}
